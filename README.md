# Kind

## Create Cluster

- `$ kind create cluster` create default a kind cluster called `kind`
- `$ kind create cluster --name kind-2` create default a kind cluster called `kind-2`
- `$ kind create cluster --name my-cluster --config kind-config.yaml` create by file

## Switch Context

- `$ kubectl config get-contexts` get contexts
- `$ kubectl config use-context {context-name}` switch context
- `$ kubectl get nodes` get nodes
- `$ kubectl get nodes --show-labels`
  - we can simply deploy into a specific node
  - or we can randomize deploy in a node


`kind create cluster --name my-cluster --config kind-config.yaml`
